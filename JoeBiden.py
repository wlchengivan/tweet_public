import pandas as pd
import urllib, json, requests
from textblob import TextBlob
import sys
import tweepy
import matplotlib.pyplot as plt
import numpy as np
import MySQLdb
import ast
import os

#twitter_key
consumerKey = "jZRviKuBKSeeu7n4siwdu3yZy"
consumerSecret = "clgcxxbzQtIzEDzpGihzPFhax94D0LJVyA8jtMeLRTnrDUZxOk"
accessToken = "942799637622042624-0BD8tTQROS9Jg0ZnCxFOYmvKTwjvy70"
accessTokenSecret = "hyjjaY0XFwT7dkDQ2u9thqXtv77GB23sTeDRyi34KFSPO"
bearer_token = "AAAAAAAAAAAAAAAAAAAAAEbSOgEAAAAABtLwj0BLbiNjVUoPbnkmcUv%2BhA4%3DZ6TTOjJuNlKYd7f1no1DV0AEXaa6wwZQ73QmT8xrsOB7PCUv9N"

#authorization 4 key
auth = tweepy.OAuthHandler(consumerKey, consumerSecret)
auth.set_access_token(accessToken, accessTokenSecret)
  
#calling the api 
api = tweepy.API(auth)

def getprofile(query):
  
    # fetching the user
    user = api.get_user(query)
  
    # fetching
    ID = user.id_str
    followers_count = user.followers_count
    friends_count = user.friends_count
    created_at = user.created_at
    description = user.description
    name = user.name
    screen_name = user.screen_name
    location = user.location

    #print(ID, followers_count, friends_count, created_at, description, name, screen_name, location)

    print("User ID: ", ID)
    print("User name: ", name)
    print("User screen name: ", screen_name)
    print(f"User have {followers_count} followers")
    print(f"User have {friends_count} friends")
    print("User description: ", description)
    print("User created at: ", created_at)
    print("User location: ", location)

    #connect db
    cursor=db.cursor()
    sql="INSERT INTO profile(user_Id, name, screen_name, followers_count, friends_count, description, created_at, location) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)"

    try:
        cursor.execute(sql, (ID, name, screen_name, followers_count, friends_count, description, created_at, location))
        db.commit()
    except:
        sql="UPDATE profile set name = %s, screen_name = %s, followers_count = %s, friends_count = %s, description = %s, created_at = %s, location= %s where user_Id = %s"
        cursor.execute(sql, (name, screen_name, followers_count, friends_count, description, created_at, location, ID))
        db.commit()

def getfriends(query):
    # fetching the friends
    friends = api.friends(query,count=50)

    # fetching
    for x in range(0, len(friends)):
        cursor=db.cursor()

        #print(friends[x].id_str, friends[x].name, friends[x].screen_name, friends[x].created_at)
        sql="INSERT INTO friends(user_Id, name, screen_name, created_at) VALUES (%s, %s, %s, %s)"
        try:
            cursor.execute(sql, (friends[x].id_str, friends[x].name, friends[x].screen_name, friends[x].created_at))
            db.commit()
        except:
            sql="UPDATE friends set name = %s, screen_name = %s, created_at = %s where user_Id = %s"
            cursor.execute(sql, (friends[x].name, friends[x].screen_name, friends[x].created_at, friends[x].id_str))
            db.commit()

def getfollowers(query):
    # fetching the followers
    followers = api.followers(query)
    #print(len(followers))

    # fetching
    for x in range(0, len(followers)):
        cursor=db.cursor()
        #print(followers[x].id_str, followers[x].name, followers[x].screen_name, followers[x].created_at)
        sql="INSERT INTO followers(user_Id, name, screen_name, created_at) VALUES (%s, %s, %s, %s)"
        
        try:
            cursor.execute(sql, (followers[x].id_str, followers[x].name, followers[x].screen_name, followers[x].created_at))
            db.commit()
        except:
            sql="UPDATE followers set name = %s, screen_name = %s, created_at = %s where user_Id = %s"
            cursor.execute(sql, (followers[x].name, followers[x].screen_name, followers[x].created_at, followers[x].id_str))
            db.commit()

def showtweet():
    cursor=db.cursor()
    sql="select * from tweets"
    cursor.execute(sql)
    result=cursor.fetchall()
    db.commit()
    pdresult = []

    for x in range(0, len(result)):
        pdresult.append(list(result[x]))

    print(pd.DataFrame(pdresult, columns=['TweetID', 'UserId', 'Context', 'CreateAt']))

def showfriendlist():
    cursor=db.cursor()
    sql="select * from friends"
    cursor.execute(sql)
    result=cursor.fetchall()

    db.commit()
    pdresult = []
    for x in range(0, len(result)):
        pdresult.append(list(result[x]))

    print(pd.DataFrame(pdresult, columns=['ID', 'Name', 'Screen Name', 'CreateAt']))

def showfollowerlist():
    cursor=db.cursor()
    sql="select * from followers"
    cursor.execute(sql)
    result=cursor.fetchall()

    db.commit()
    pdresult = []
    for x in range(0, len(result)):
        pdresult.append(list(result[x]))

    print(pd.DataFrame(pdresult, columns=['ID', 'Name', 'Screen Name', 'CreateAt']))

def search_twitter2(query):
    tweets = api.search(query, count=20)

    for x in range(0,len(tweets)):
        cursor=db.cursor()
        sql = "INSERT INTO tweets(author_id, created_at, tweet_id, text) VALUES (%s, %s, %s, %s)"

        try: 
            cursor.execute(sql, (tweets[x].user.id, tweets[x].created_at, tweets[x].id, tweets[x].text))
            db.commit()
        except:
            sql = "UPDATE tweets set author_id = %s, created_at = %s, text = %s where tweet_id = %s"
            cursor.execute(sql, (tweets[x].user.id, tweets[x].created_at, tweets[x].text, tweets[x].id))
            db.commit()

def menu():
    while True:
        print("Can i help you?\n1: Get Joe Biden information.\n2: Get Joe Biden friend list.\n3: Get Joe Biden follower list.\n4: Search with keyword <Coronavirus>\n5: Search with keyword <Vaccination>\n6: Show Joe Biden friend list\n7: Show Joe Biden follower list\n8: Show tweet\n0: Exit")
        task=input()
        if task == '1':
            getprofile("JoeBiden")
            os.system("pause")
        elif task == '2':
            getfriends("JoeBiden")
            print("JoeBiden's friend list was saved")
            os.system("pause")
        elif task == '3':
            getfollowers("JoeBiden")
            print("JoeBiden's follower was saved")
            os.system("pause")
        elif task == '4':
            #inserttweet(search_twitter("Coronavirus"))
            search_twitter2("Coronavirus")
            print("Tweet was saved")
            os.system("pause")
        elif task == '5':
            #inserttweet(search_twitter("Vaccination"))
            search_twitter2("Vaccination")
            print("Tweet was saved")
            os.system("pause")
        elif task == '6':
            showfriendlist()
            os.system("pause")
        elif task == '7':
            showfollowerlist()
            os.system("pause")
        elif task == '8':
            showtweet()
            os.system("pause")
        elif task == '0':
            print("Goodbye")
            break
        else:
            print("Please input 1-8")

#connect db
try:
    db=MySQLdb.connect(host="127.0.0.1",user="root", passwd="", db="jdeproject", charset = "utf8mb4") 
    menu()
except:
    print("Can't connect to MySQL server")